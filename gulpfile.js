var gulpversion = '4';
var gulp         = require("gulp"),
    gutil        = require("gulp-util"),
    pug          = require("gulp-pug"),
    sass         = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    nano         = require('gulp-cssnano'),
    shorthand    = require('gulp-shorthand'),
    //uncss        = require('gulp-uncss'),
    cleancss     = require("gulp-clean-css"),
    browserSync  = require("browser-sync"),
    fileSize     = require("gulp-filesize"),
    rename       = require("gulp-rename"),
    notify       = require("gulp-notify"),
    uglify       = require("gulp-uglify");
    //concat       = require('gulp-concat'),
    //clean        = require("gulp-clean");

gulp.task("browser-sync", function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false,
  })
});

/* gulp.task("clean", function() {
  return gulp.src("app/")
  .pipe(clean())
}); */

gulp.task("css", function() {
  return gulp.src("src/sass/**/*.sass")
  .pipe(sass().on("error", sass.logError))
  .pipe(autoprefixer(["last 15 versions"]))
  .pipe(gulp.dest('app/css'))
  .pipe(fileSize())
  .pipe(shorthand())
  .pipe(cleancss( {level: { 2: { removeDuplicateRules: true} } } ))
  .pipe(nano())
  .pipe(rename( {suffix: ".min", prefix: ""} ))
  .pipe(gulp.dest('app/css'))
  .pipe(fileSize())
  .pipe(browserSync.stream())
});

gulp.task("html", function() {
  return gulp.src("src/**/*.pug")
  .pipe(pug())
  .pipe(gulp.dest("app/"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("img", function() {
  return gulp.src("src/img/**/*.*")
  .pipe(gulp.dest("app/img/"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("fonts", function() {
  return gulp.src("src/fonts/**/*.*")
  .pipe(gulp.dest("app/fonts/"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("js", function() {
  return gulp.src("src/js/**/*.js")
  //.pipe(uglify())
  .pipe(gulp.dest("app/js"))
  .pipe(browserSync.reload({ stream: true }))
});

gulp.task("watch", function() {
  gulp.watch("src/sass/**/*.sass", gulp.parallel("css"))
  gulp.watch("src/img/**/*.*", gulp.parallel("img"))
  gulp.watch("src/fonts/**/*.*", gulp.parallel("fonts"))
  gulp.watch("src/*.pug", gulp.parallel("html"))
  gulp.watch("src/js/*.js", gulp.parallel("js"))
});

gulp.task("default", gulp.parallel("css", "img", "fonts", "html", "js", "browser-sync", "watch"));
