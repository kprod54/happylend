const sliderPic = document.querySelector('.slider__item');
window.addEventListener('resize', function () {
  if (sliderPic.width > window.innerWidth) {
    sliderPic.style.visibility = 'hidden';
  } else {
    sliderPic.style.visibility = 'visible';
  }
})
